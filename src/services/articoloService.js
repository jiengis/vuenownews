import { apiUrl } from "./globals";

export const path = "/articoli";

export function addArticolo(articolo) {
  fetch(apiUrl + path, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(articolo)
  }).then(response => {
    console.log(response);
  });
}

// export function getArticoli() {
//   fetch(apiUrl + path)
//     .then(response => {
//       return response.json();
//     })
//     .then(response => {
//       console.log(response);
//     });
// }

export function deleteArticolo(id) {
  fetch(apiUrl + path + "/" + id, {
    method: "DELETE"
  })
    .then(response => {
      return response.json();
    })
    .then(response => {
      console.log(response);
    });
}

/*    UPDATE
 export function updateArticolo(articolo,id) {
   fetch(apiUrl + path + "/" + id, {
     method: "PUT",
     headers: {
       Accept: "application/json",
       "Content-Type": "application/json"
     },
     body: JSON.stringify(articolo)
   }).then(response => {
     console.log(response);
   });
 }
*/
