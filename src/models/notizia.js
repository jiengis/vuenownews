export let notizia = {
  id: 0,
  titolo: "",
  testo: "",
  immagine: "",
  dataCreazione: null,
  dataPubblicazione: null,
  sottotitolo: "",
  riassunto: "",
  dataFineVisual: null,
  importanza: "",
  autoreNotizia: 0,
  commentiNotizia: []
};
