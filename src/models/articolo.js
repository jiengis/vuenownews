export let articolo = {
  id: 0,
  titolo: "",
  testo: "",
  immagine: "",
  dataCreazione: null,
  dataPubblicazione: null,
  sezione: "",
  prezzo: 0,
  vendibile: false,
  autoreArticolo: 0
};
