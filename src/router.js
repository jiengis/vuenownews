import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/articoli",
      name: "articoli",
      component: () => import("./views/Articoli.vue")
    },
    {
      path: "/articoli-new",
      name: "createArticolo",
      component: () => import("./components/CreateArticolo.vue")
    },
    {
      path: "/notizie",
      name: "notizie",
      component: () => import("./views/Notizie.vue")
    },
    {
      path: "/notizie-new",
      name: "createNotizia",
      component: () => import("./components/CreateNotizia.vue")
    }
  ]
});
